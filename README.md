# SSH in browser over https and http

- This work on FreeBSD, Debian/Ubuntu, Fedora

- For installation:

* `sudo apt-get install -y git`
* `git clone https://github.com/krlex/browser-ssh`
*  `./script.sh`


Video Demo(Freebsd):

[![asciicast](https://asciinema.org/a/Uw7cbtz2TWUKSbQedLqxjuCFL.svg)](https://asciinema.org/a/Uw7cbtz2TWUKSbQedLqxjuCFL)
